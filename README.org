* notes for further work
- refactor system to make it easier to reuse
- envsubst in nginx container, rather than mapping in config.js
- figure out how to work around port 80 problemet in nginx and
  openshift, alternatively document a pattern that uses port 8000
- make names more general, remove "taxonomy"
- parameter file, with route and so on
- figure out ssl cert management(solved)
- project name parameter
- create namespace if it doesnt exist
- promotion chain, with environment names, for example dev i1 prod etc
- easy promotion interface
- similar interface regardless of deployment system

- manage rollback, back through history
- separate build and deploy
- manage several clusters, dev and prod for example
- separate secrets for different clusters, dev and prod
- manage auto build on commit
- manage automatic tests before auto promotion

- manual triggers as well as automatic
- investigate local environments, minikube, code-ready-containers
- manage logging, probing, health checks, etc
- lint of configuration
- manage autoscaling yes/no

- investigate using operator pattern, or tekton
- how do we manage several branches?
- how do we use git tagging to keep track of deploys?

- oc delete project from makefile
