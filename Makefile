#
# make deploy-test - run kustomize and oc apply on all test environments.
# make deploy-prod - run kustomize and oc apply on prod.
#
#
.ONESHELL:


## Determine which cluster we are logged in to
CLUSTER = $(shell env TERM=dumb LANG=C oc cluster-info | grep "running at" | sed 's|^.*running at ||')
ifneq ($(CLUSTER), "")
$(info Operating on $(CLUSTER).)
else
$(error You must be logged in to an OpenShift cluster.)
endif

## Determine which git branch is currently in use
BRANCH  = $(shell LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)

TESTCLUSTER = "https://api.test.services.jtech.se:6443"
PRODCLUSTER = "https://api.prod.services.jtech.se:6443"

## The name of the directory containing the prod configuration
PRODENVDIR = k8s/prod

## This env is handled by tekton/build.yaml and deployed via webhooks.
DEVENVDIR  = k8s/dev

## The directories containing the test environment configurations
TESTENVDIRS = $(shell find k8s -mindepth 1 -maxdepth 1 ! -path . -type d | grep -v base | grep -v $(PRODENVDIR) | grep -v $(DEVENVDIR))


PROJECT = jocke-slask-1

# Usage: make set-api-image SHA=35fe8ad53bad8358f774cf557e1ce4c799238315
# Sets the deployment image for i1, u1 and frontend
set-api-image:
	@if [ -z "$(SHA)" ]; then
		echo "**** error: env variable SHA not set"
		exit 1
	else
		sed -i.old "s/jobtech@sha256:.*/jobtech@sha256:$(SHA)/" k8s/test/set-deployment-image.yaml
	fi

deploy-test: check-logged-in gitcheck setup-test
	@if [ "$(CLUSTER)" != "$(TESTCLUSTER)" ]; then
		echo "**** error: not logged in to $(TESTCLUSTER)  $(CLUSTER)"
		exit 1
	fi
	for ENV in $(TESTENVDIRS); do
		cp jobtech-taxonomy-api-deploy-secrets/cert/taxonomy-api-jobtechdev-se.yaml $$ENV/taxonomy-api-jobtechdev-se.yaml
		kustomize build $$ENV | oc apply -f -
		rm -f $$ENV/taxonomy-api-jobtechdev-se.yaml
	done


deploy-prod: check-logged-in gitcheck setup-prod
	@if [ "$(CLUSTER)" != "$(PRODCLUSTER)" ]; then
		echo "**** error: not logged in to $(PRODCLUSTER)"
		exit 1
	fi
	cp jobtech-taxonomy-api-deploy-secrets/cert/taxonomy-api-jobtechdev-se.yaml $(PRODENVDIR)/taxonomy-api-jobtechdev-se.yaml
	kustomize build $(PRODENVDIR) | oc apply -f -
	rm -f $(PRODENVDIR)/taxonomy-api-jobtechdev-se.yaml


build-test: setup-test
	TMPF=$$(mktemp)
	tkn pipeline start --resource gitops-repo=jobtech-taxonomy-gitops-repo \
			   --resource git-repo=jobtech-taxonomy-public-repo \
			   --resource image=jobtech-taxonomy-public-image  \
			   build-and-deploy-api | tee $$TMPF
	LOGCMD=$$(grep "tkn pipelinerun logs" $$TMPF)
	$$LOGCMD
	rm -f $$TMPF


#### Helper targets - not to be run from command line directly #####

setup-test: check-project
	oc apply -f jobtech-taxonomy-api-deploy-secrets/nexus/nexus.yaml
	oc apply -f config/service-accounts.yaml
	oc apply -f tekton/build.yaml


setup-prod: check-project
	kustomize build jobtech-taxonomy-api-deploy-secrets/prod/ | oc apply -f -
	oc apply -f config/prod/datomic-prod.yaml
	oc apply -f jobtech-taxonomy-api-deploy-secrets/nexus/nexus.yaml
	oc apply -f config/service-accounts.yaml

check-project:
	oc new-project $(PROJECT) || oc project $(PROJECT)
	@if [ "$$(oc project -q)" != "$(PROJECT)" ]; then
		echo "**** error: failed to switch to $(PROJECT)"
		exit 1
	fi

delete-project:
	oc delete project $(PROJECT)

gitcheck:
	@if [ ! -z "$$(git status --porcelain)" ]; then
		echo "**** error: repo is not clean"
		exit 1
	fi
	if [ "$(BRANCH)" != "master" ]; then
		echo "**** error: not on master ($(BRANCH))"
		exit 1
	fi
	git pull

check-logged-in:
	if [ -z "$(CLUSTER)" ]; then
		exit 1
	fi
